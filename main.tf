locals {
  bucket_arn = coalesce(var.s3_bucket_arn, join("", aws_s3_bucket.cloudwatch_logs.*.arn))
  bucket_id  = coalesce(regex("([^:]*)$", var.s3_bucket_arn)[0], join("", aws_s3_bucket.cloudwatch_logs.*.id))

  tags = merge(
    {
      Name = var.name
    },
    var.tags
  )
}

resource "aws_s3_bucket" "cloudwatch_logs" {
  count = var.s3_bucket_arn == "" ? 1 : 0

  bucket = var.name
  tags   = local.tags
}

resource "aws_s3_bucket_server_side_encryption_configuration" "cloudwatch_logs" {
  count = var.s3_bucket_arn == "" ? 1 : 0

  bucket = aws_s3_bucket.cloudwatch_logs[0].id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_cloudwatch_log_group" "log_processor" {
  name              = "/aws/lambda/${var.name}"
  retention_in_days = var.log_retention
  tags              = local.tags
}

data "archive_file" "log_processor" {
  type        = "zip"
  source_file = "${path.module}/lambda/log_processor.py"
  output_path = "${path.module}/lambda/log_processor.zip"
}

resource "aws_lambda_function" "log_processor" {
  function_name    = var.name
  description      = "An Amazon Kinesis Firehose stream processor that extracts individual log events from records sent by Cloudwatch Logs subscription filters."
  filename         = data.archive_file.log_processor.output_path
  source_code_hash = data.archive_file.log_processor.output_base64sha256
  role             = aws_iam_role.log_processor.arn
  runtime          = "python3.8"
  handler          = "log_processor.lambda_handler"
  timeout          = "60"
  tags             = local.tags
}

resource "aws_kinesis_firehose_delivery_stream" "s3" {
  name        = var.name
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn            = aws_iam_role.firehose.arn
    bucket_arn          = local.bucket_arn
    prefix              = var.s3_prefix
    error_output_prefix = var.s3_output_error_prefix
    buffer_size         = 64

    processing_configuration {
      enabled = true

      processors {
        type = "Lambda"

        parameters {
          parameter_name  = "LambdaArn"
          parameter_value = "${aws_lambda_function.log_processor.arn}:$LATEST"
        }

        dynamic "parameters" {
          for_each = var.lambda_processor_parameters

          content {
            parameter_name  = parameters.value.parameter_name
            parameter_value = parameters.value.parameter_value
          }
        }
      }
    }

    dynamic_partitioning_configuration {
      enabled = true
    }
  }

  server_side_encryption {
    enabled = true
  }

  tags = local.tags
}

resource "aws_iam_role" "log_processor" {
  name               = "${var.name}-log-processor"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume.json

  tags = local.tags
}

data "aws_iam_policy_document" "lambda_assume" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "log_processor" {
  statement {
    effect = "Allow"

    actions = ["logs:CreateLogGroup"]

    resources = [
      "arn:${data.aws_partition.current.partition}:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["arn:${data.aws_partition.current.partition}:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${var.name}:*"]
  }

  statement {
    effect = "Allow"

    actions = ["firehose:PutRecordBatch"]

    resources = ["arn:${data.aws_partition.current.partition}:firehose:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:deliverystream/${var.name}"]
  }
}

resource "aws_iam_role_policy" "log_processor" {
  name   = "${var.name}-log-processor"
  role   = aws_iam_role.log_processor.name
  policy = data.aws_iam_policy_document.log_processor.json
}

resource "aws_iam_role" "cloudwatch_logs" {
  name               = "${var.name}-cloudwatch-logs"
  assume_role_policy = data.aws_iam_policy_document.cloudwatch_logs_assume.json

  tags = local.tags
}

data "aws_iam_policy_document" "cloudwatch_logs_assume" {
  statement {
    effect = "Allow"

    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"

      identifiers = ["logs.${data.aws_region.current.name}.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "cloudwatch_logs_firehose" {
  statement {
    effect = "Allow"

    actions = ["firehose:*"]

    resources = [aws_kinesis_firehose_delivery_stream.s3.arn]
  }
}

resource "aws_iam_role_policy" "cloudwatch_logs_firehose" {
  name   = "${var.name}-cloudwatch-logs-firehose"
  role   = aws_iam_role.cloudwatch_logs.name
  policy = data.aws_iam_policy_document.cloudwatch_logs_firehose.json
}

resource "aws_iam_role" "firehose" {
  name               = "${var.name}-firehose"
  assume_role_policy = data.aws_iam_policy_document.firehose_assume.json

  tags = local.tags
}

data "aws_iam_policy_document" "firehose_assume" {
  statement {
    effect = "Allow"

    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"

      identifiers = ["firehose.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "firehose" {
  statement {
    effect = "Allow"

    actions = [
      "lambda:InvokeFunction",
      "lambda:GetFunctionConfiguration",
    ]

    resources = [
      aws_lambda_function.log_processor.arn,
      "${aws_lambda_function.log_processor.arn}:*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "kinesis:DescribeStream",
      "kinesis:GetShardIterator",
      "kinesis:GetRecords",
    ]

    resources = [aws_kinesis_firehose_delivery_stream.s3.arn]
  }


  statement {
    effect = "Allow"

    actions = [
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:PutObject",
    ]

    resources = [
      "${local.bucket_arn}/*",
      local.bucket_arn
    ]
  }

}

resource "aws_iam_role_policy" "firehose" {
  name   = "${var.name}-firehose"
  role   = aws_iam_role.firehose.name
  policy = data.aws_iam_policy_document.firehose.json
}
