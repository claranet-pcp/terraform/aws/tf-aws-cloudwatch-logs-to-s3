output "kinesis_firehose_delivery_stream_arn" {
  description = "Kinesis Firehose delivery stream ARN to use with CloudWatch Logs subscription filters"
  value       = aws_kinesis_firehose_delivery_stream.s3.arn
}

output "iam_role_arn" {
  description = "IAM role ARN to use with CloudWatch Logs subscription filters"
  value       = aws_iam_role.cloudwatch_logs.arn
}

output "s3_bucket_arn" {
  description = "Log bucket ARN"
  value       = local.bucket_arn
}

output "s3_bucket_id" {
  description = "Log bucket ID"
  value       = local.bucket_id
}
