variable "name" {
  type        = string
  description = "Name to assign created resources"
}

variable "lambda_processor_parameters" {
  type        = list(map(string))
  description = "List of parameters to apply to the Lambda processor function"
  default     = []
}

variable "log_retention" {
  type        = string
  description = "Lambda processor log retention in days"
  default     = 30
}

variable "s3_bucket_arn" {
  type        = string
  description = "ARN of an existing S3 bucket to use"
  default     = ""
}

variable "s3_prefix" {
  type        = string
  description = "Prefix for logs in S3. Supports 'logGroup', 'logStream' and 'owner' Lambda partition key variables along with standard custom prefixes"
  default     = "!{partitionKeyFromLambda:logGroup}/!{partitionKeyFromLambda:logStream}/"
}

variable "s3_output_error_prefix" {
  type        = string
  description = "Error prefix for logs in S3. Support standard custom prefixes"
  default     = "!{firehose:error-output-type}/"
}

variable "tags" {
  type        = map(string)
  description = "Map of tags to add to resources"
  default     = {}
}
